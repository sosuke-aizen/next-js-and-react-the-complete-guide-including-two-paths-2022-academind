

This course was recorded with `next-auth` v3. Currently, the latest version is
v4. It generally works the same as shown in the lectures but some imports and
method names changed.

Therefore, I recommend that you **follow along with v3** for a smooth
experience _(this command needs to be executed as part of the next lecture)_ :

    
    
    npm install --save-exact next-auth@3

You can, optionally, also dive into the official v4 migration guide to update
your code to `next-auth` v4, if you want to: <https://next-
auth.js.org/getting-started/upgrade-v4>

