

In the next lecture, we'll install an **extra package** that helps us add
**"Routing"**.

You will learn more about it in the next lecture but **to follow along
smoothly,** make sure you install the **correct version** _(when we install
the package in the next lecture)_ by running:

    
    
    npm install --save react-router-dom@5

