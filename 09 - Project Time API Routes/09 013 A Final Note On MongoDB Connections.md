

In this course, we always close our MongoDB connections via `client.close()`.

This works and you can do that.

If you build an application where your MongoDB-related code will execute
**frequently** (e.g. the API route will be hit frequently), you might want to
take advantage of MongoDB's " **connection pooling** " though.

For this, simply remove all `client.close()` calls from your code. The
connection will then NOT be closed and will be re-used across requests.

