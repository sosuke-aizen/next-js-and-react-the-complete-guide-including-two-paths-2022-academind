# Next.js & React - The Complete Guide (incl. Two Paths!) 2022 - Academind

## 01 - Getting Started/:

- 01 001 Welcome to the Course!.mp4
- 01 002 What Is Next.js And Why Would You Use It.mp4
- 01 003 Key Feature Server-side Page (Pre-)Rendering.mp4
- 01 004 Key Feature File-based Routing.mp4
- 01 005 Key Feature Build Fullstack React Apps!.mp4
- 01 006 Join Our Learning Community!.html
- 01 007 Creating a NextJS Project & Setting Up Our IDE.mp4
- 01 008 Analyzing The Created Project.mp4
- 01 009 A First Demo NextJS In Action!.mp4
- 01 010 About This Course & Course Outline.mp4
- 01 011 Taking This Course Your Two Options.mp4
- 01 012 How To Get The Most Out Of This Course.mp4
- 01 013 Downloading Course Code.html
- 01 014 Module Resources.html
01 external-assets-links.txt

## 02 - Optional React Refresher/:

- 02 001 Module Introduction.mp4
- 02 002 What is ReactJS.mp4
- 02 003 Why ReactJS & A First Demo.mp4
- 02 004 Building Single-Page Applications (SPAs).mp4
- 02 005 React Alternatives.mp4
- 02 006 Creating a New React Project.mp4
- 02 007 Setting Up A Code Editor.mp4
- 02 008 React 18.html
- 02 009 Diving Into The Created Project.mp4
- 02 010 How React Works & Understanding Components.mp4
- 02 011 More Component Work & Styling With CSS Classes.mp4
- 02 012 Building & Re-using Components.mp4
- 02 013 Passing Data With Props & Dynamic Content.mp4
- 02 014 Handling Events.mp4
- 02 015 Adding More Components.mp4
- 02 016 Introducing State.mp4
- 02 017 Working with Event Props.mp4
- 02 018 Use The Right React Router Version.html
- 02 019 Adding Routing.mp4
- 02 020 Adding Links & Navigation.mp4
- 02 021 Scoping Component Styles With CSS Modules.mp4
- 02 022 Outputting Lists Of Data & Components.mp4
- 02 023 Adding Even More Components.mp4
- 02 024 Creating Wrapper Components.mp4
- 02 025 Working With Forms.mp4
- 02 026 Getting User Input & Handling Form Submission.mp4
- 02 027 Preparing The App For Http Requests & Adding a Backend.mp4
- 02 028 Sending a POST Http Request.mp4
- 02 029 Navigating Programmatically.mp4
- 02 030 Getting Started with Fetching Data.mp4
- 02 031 Using the useEffect Hook.mp4
- 02 032 Introducing React Context.mp4
- 02 033 Updating State Based On Previous State.mp4
- 02 034 Using Context In Components.mp4
- 02 035 More Context Usage.mp4
- 02 036 Module Summary.mp4
- 02 037 Module Resources.html
02 external-assets-links.txt

## 03 - Pages & File-based Routing/:

- 03 001 Module Introduction.mp4
- 03 002 Our Starting Setup.mp4
- 03 003 What Is File-based Routing And Why Is It Helpful.mp4
- 03 004 Adding A First Page.mp4
- 03 005 Adding a Named Static Route File.mp4
- 03 006 Working with Nested Paths & Routes.mp4
- 03 007 Adding Dynamic Paths & Routes.mp4
- 03 008 Extracting Dynamic Path Segment Data (Dynamic Routes).mp4
- 03 009 Building Nested Dynamic Routes & Paths.mp4
- 03 010 Adding Catch-All Routes.mp4
- 03 011 Navigating with the Link Component.mp4
- 03 012 Navigating To Dynamic Routes.mp4
- 03 013 A Different Way Of Setting Link Hrefs.mp4
- 03 014 Navigating Programmatically.mp4
- 03 015 Adding a Custom 404 Page.mp4
- 03 016 Module Summary.mp4
- 03 017 Module Resources.html
03 external-assets-links.txt

## 04 - Project Time Working with File-based Routing/:

- 04 001 Module Introduction.mp4
- 04 002 Planning The Project.mp4
- 04 003 Setting Up The Main Pages.mp4
- 04 004 Adding Dummy Data & Static Files.mp4
- 04 005 Adding Regular React Components.mp4
- 04 006 Adding More React Components & Connecting Components.mp4
- 04 007 Styling Components In Next.js Projects.mp4
- 04 008 Adding Buttons & Icons.mp4
- 04 009 Adding the Event Detail Page (Dynamic Route).mp4
- 04 010 Adding a General Layout Wrapper Component.mp4
- 04 011 Working on the All Events Page.mp4
- 04 012 Adding a Filter Form for Filtering Events.mp4
- 04 013 Navigating to the Filtered Events Page Progammatically.mp4
- 04 014 Extracting Data on the Catch-All Page.mp4
- 04 015 Final Steps.mp4
- 04 016 Module Summary.mp4
- 04 017 Module Resources.html
04 external-assets-links.txt

## 05 - Page Pre-Rendering & Data Fetching/:

- 05 001 Module Introduction.mp4
- 05 002 The Problem With Traditional React Apps (and Data Fetching).mp4
- 05 003 How NextJS Prepares & Pre-renders Pages.mp4
- 05 004 Introducing Static Generation with getStaticProps.mp4
- 05 005 NextJS Pre-renders By Default!.mp4
- 05 006 Adding getStaticProps To Pages.mp4
- 05 007 Running Server-side Code & Using the Filesystem.mp4
- 05 008 A Look Behind The Scenes.mp4
- 05 009 Utilizing Incremental Static Generation (ISR).mp4
- 05 010 ISR A Look Behind The Scenes.mp4
- 05 011 A Closer Look At getStaticProps & Configuration Options.mp4
- 05 012 Working With Dynamic Parameters.mp4
- 05 013 Introducing getStaticPaths For Dynamic Pages.mp4
- 05 014 Using getStaticPaths.mp4
- 05 015 getStaticPaths & Link Prefetching Behind The Scenes.mp4
- 05 016 Working With Fallback Pages.mp4
- 05 017 Loading Paths Dynamically.mp4
- 05 018 Fallback Pages & Not Found Pages.mp4
- 05 019 Introducing getServerSideProps for Server-side Rendering (SSR).mp4
- 05 020 Using getServerSideProps for Server-side Rendering.mp4
- 05 021 getServerSideProps and its Context.mp4
- 05 022 Dynamic Pages & getServerSideProps.mp4
- 05 023 getServerSideProps Behind The Scenes.mp4
- 05 024 Introducing Client-Side Data Fetching (And When To Use It).mp4
- 05 025 Implementing Client-Side Data Fetching.mp4
- 05 026 A Note About useSWR.html
- 05 027 Using the useSWR NextJS Hook.mp4
- 05 028 Combining Pre-Fetching With Client-Side Fetching.mp4
- 05 029 Module Summary.mp4
- 05 030 Module Resources.html
05 external-assets-links.txt

## 06 - Project Time Page Pre-rendering & Data Fetching/:

- 06 001 Module Introduction.mp4
- 06 002 Preparations.mp4
- 06 003 Adding Static Site Generation (SSG) On The Home Page.mp4
- 06 004 Loading Data & Paths For Dynamic Pages.mp4
- 06 005 Optimizing Data Fetching.mp4
- 06 006 Working on the All Events Page.mp4
- 06 007 Using Server-side Rendering (SSR).mp4
- 06 008 Adding Client-Side Data Fetching.mp4
- 06 009 Module Summary.mp4
- 06 010 Module Resources.html
06 external-assets-links.txt

## 07 - Optimizing NextJS Apps/:

- 07 001 Module Summary.mp4
- 07 002 Analyzing the Need for head Metadata.mp4
- 07 003 Configuring the head Content.mp4
- 07 004 Adding Dynamic head Content.mp4
- 07 005 Reusing Logic Inside A Component.mp4
- 07 006 Working with the _app.js File (and Why).mp4
- 07 007 Merging head Content.mp4
- 07 008 The _document.js File (And What It Does).mp4
- 07 009 A Closer Look At Our Images.mp4
- 07 010 Optimizing Images with the Next Image Component & Feature.mp4
- 07 011 Taking A Look At The Next Image Documentation.mp4
- 07 012 Module Summary.mp4
- 07 013 Module Resources.html
07 external-assets-links.txt

## 08 - Adding Backend Code with API Routes (Fullstack React)/:

- 08 001 Module Introduction.mp4
- 08 002 What are API Routes.mp4
- 08 003 Writing Our First API Route.mp4
- 08 004 Preparing the Frontend Form.mp4
- 08 005 Parsing The Incoming Request & Executing Server-side Code.mp4
- 08 006 Sending Requests To API Routes.mp4
- 08 007 Using API Routes To Get Data.mp4
- 08 008 Using API Routes For Pre-Rendering Pages.mp4
- 08 009 Creating & Using Dynamic API Routes.mp4
- 08 010 Exploring Different Ways Of Structuring API Route Files.mp4
- 08 011 Module Summary.mp4
- 08 012 Module Resources.html
08 external-assets-links.txt

## 09 - Project Time API Routes/:

- 09 001 Module Introduction.mp4
- 09 002 Starting Setup & A Challenge For You!.mp4
- 09 003 Adding a Newsletter Route.mp4
- 09 004 Adding Comments API Routes.mp4
- 09 005 Connecting the Frontend To the Comments API Routes.mp4
- 09 006 Setting Up A MongoDB Database.mp4
- 09 007 Running MongoDB Queries From Inside API Routes.mp4
- 09 008 Inserting Comments Into The Database.mp4
- 09 009 Getting Data From The Database.mp4
- 09 010 Improvement Getting Comments For A Specific Event.html
- 09 011 Adding Error Handling.mp4
- 09 012 More Error Handling.mp4
- 09 013 A Final Note On MongoDB Connections.html
- 09 014 Module Summary.mp4
- 09 015 Module Resources.html
09 external-assets-links.txt

## 10 - Working with App-wide State (React Context)/:

- 10 001 Module Introduction.mp4
- 10 002 Our Target State & Starting Project.mp4
- 10 003 Creating a New React Context.mp4
- 10 004 Adding Context State.mp4
- 10 005 Using Context Data In Components.mp4
- 10 006 Example Triggering & Showing Notifications.mp4
- 10 007 Example Removing Notifications (Automatically).mp4
- 10 008 Challenge Solution.mp4
- 10 009 Module Summary.mp4
- 10 010 Module Resources.html
10 external-assets-links.txt

## 11 - Complete App Example Build a Full Blog A to Z/:

- 11 001 Module Introduction.mp4
- 11 002 Setting Up The Core Pages.mp4
- 11 003 Getting Started With The Home Page.mp4
- 11 004 Adding The Hero Component.mp4
- 11 005 Adding Layout & Navigation.mp4
- 11 006 Time To Add Styling & A Logo.mp4
- 11 007 Starting Work On The Featured Posts Part.mp4
- 11 008 Adding A Post Grid & Post Items.mp4
- 11 009 Rendering Dummy Post Data.mp4
- 11 010 Adding the All Posts Page.mp4
- 11 011 Working On The Post Detail Page.mp4
- 11 012 Rendering Markdown As JSX.mp4
- 11 013 Adding Markdown Files As A Data Source.mp4
- 11 014 Adding Functions To Read & Fetch Data From Markdown Files.mp4
- 11 015 Using Markdown Data For Rendering Posts.mp4
- 11 016 Rendering Dynamic Post Pages & Paths.mp4
- 11 017 Rendering Custom HTML Elements with React Markdown.html
- 11 018 Rendering Images With The Next Image Component (From Markdown).mp4
- 11 019 Rendering Code Snippets From Markdown.mp4
- 11 020 Preparing The Contact Form.mp4
- 11 021 Adding The Contact API Route.mp4
- 11 022 Sending Data From The Client To The API Route.mp4
- 11 023 Storing Messages With MongoDB In A Database.mp4
- 11 024 Adding UI Feedback With Notifications.mp4
- 11 025 Adding head Data.mp4
- 11 026 Adding A _document.js File.mp4
- 11 027 Using React Portals.mp4
- 11 028 Module Summary.mp4
- 11 029 Module Resources.html
11 external-assets-links.txt

## 12 - Deploying NextJS Apps/:

- 12 001 Module Introduction.mp4
- 12 002 Building NextJS Apps Your Options.mp4
- 12 003 Key Deployment Steps.mp4
- 12 004 Checking & Optimizing Our Code.mp4
- 12 005 The NextJS Config File & Working With Environment Variables.mp4
- 12 006 Running a Test Build & Reducing Code Size.mp4
- 12 007 A Full Deployment Example (To Vercel).mp4
- 12 008 A Note On Github & Secret Credentials.mp4
- 12 009 Using the export Feature.mp4
- 12 010 Module Summary.mp4
- 12 011 Module Resources.html
12 external-assets-links.txt

## 13 - Adding Authentication/:

- 13 001 Module Introduction.mp4
- 13 002 Our Starting Project.mp4
- 13 003 How Does Authentication Work (In React & NextJS Apps).mp4
- 13 004 Must Read Install the Right next-auth Version.html
- 13 005 Using The next-auth Library.mp4
- 13 006 Adding A User Signup API Route.mp4
- 13 007 Sending Signup Requests From The Frontend.mp4
- 13 008 Improving Signup With Unique Email Addresses.mp4
- 13 009 Adding the Credentials Auth Provider & User Login Logic.mp4
- 13 010 Sending a Signin Request From The Frontend.mp4
- 13 011 Managing Active Session (On The Frontend).mp4
- 13 012 Adding User Logout.mp4
- 13 013 Adding Client-Side Page Guards (Route Protection).mp4
- 13 014 Adding Server-Side Page Guards (And When To Use Which Approach).mp4
- 13 015 Protecting the Auth Page.mp4
- 13 016 Using the next-auth Session Provider Component.mp4
- 13 017 Analyzing Further Authentication Requirements.mp4
- 13 018 Protecting API Routes.mp4
- 13 019 Adding the Change Password Logic.mp4
- 13 020 Sending a Change Password Request From The Frontend.mp4
- 13 021 Module Summary & Final Steps.mp4
- 13 022 Module Resources.html
13 external-assets-links.txt

## 14 - Optional NextJS Summary/:

- 14 001 Module Introduction.mp4
- 14 002 What is NextJS.mp4
- 14 003 Key Feature Server-side (Pre-) Rendering of Pages.mp4
- 14 004 Key Feature File-based Routing.mp4
- 14 005 Key Feature Build Fullstack Apps With Ease.mp4
- 14 006 Creating a NextJS Project & IDE Setup.mp4
- 14 007 Analyzing the Created Project.mp4
- 14 008 Adding First Pages To The Project.mp4
- 14 009 Adding Nested Pages Paths.mp4
- 14 010 Creating Dynamic Pages.mp4
- 14 011 Extracting Dynamic Route Data.mp4
- 14 012 Linking Between Pages.mp4
- 14 013 Onwards To A Bigger Project!.mp4
- 14 014 Preparing Our Project Pages.mp4
- 14 015 Rendering A List Of (Dummy) Meetups.mp4
- 14 016 Adding A Form For Adding Meetups.mp4
- 14 017 The _app.js File & Wrapper Components.mp4
- 14 018 Programmatic Navigation.mp4
- 14 019 Adding Custom Components & Styling With CSS Modules.mp4
- 14 020 How NextJS Page Pre-Rendering Actually Works.mp4
- 14 021 Introducing Data Fetching For Page Generation (getStaticProps).mp4
- 14 022 More Static Site Generation (SSG) With getStaticProps.mp4
- 14 023 Exploring getServerSideProps.mp4
- 14 024 Working With Dynamic Path Params In getStaticProps.mp4
- 14 025 Dynamic Pages & getStaticProps & getStaticPaths.mp4
- 14 026 Introducing API Routes.mp4
- 14 027 Connecting & Querying a MongoDB Database.mp4
- 14 028 Sending HTTP Requests To API Routes.mp4
- 14 029 Getting Data From The Database (For Page Pre-Rendering).mp4
- 14 030 Getting Meetup Detail Data & Paths.mp4
- 14 031 Adding head Metadata To Pages.mp4
- 14 032 Deploying NextJS Projects.mp4
- 14 033 Working With Fallback Pages & Re-Deploying.mp4
- 14 034 Module Summary.mp4
- 14 035 Module Resources.html
14 external-assets-links.txt

## 15 - Course Roundup/:

- 15 001 Course Roundup.mp4
- 15 002 Bonus!.html
