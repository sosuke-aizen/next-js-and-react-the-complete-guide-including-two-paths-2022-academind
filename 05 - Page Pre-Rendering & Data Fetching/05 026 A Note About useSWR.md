In the next lecture, we'll use the `useSWR` hook.

This hook generally still works as explained, but there is **one adjustment
you'll have to make**.

You must now add a default "fetcher" when working with useSWR:

    
    
    useSWR(<request-url>, (url) => fetch(url).then(res => res.json()))

The details will be explained in the next lecture, but if you run into issues
using this hook, make the change mentioned above!

