

Got stuck? Have an error in your code? Can't reproduce the results shown in
the lectures?

Compare your code to mine - below you find links to my code.

 **Important** : You can **switch between different "code snapshots"**  by
clicking on "X Commits" _(where X is a number)_

![](https://img-c.udemycdn.com/redactor/raw/article_lecture/2021-02-26_15-23-24-6211145e180fa9836d575af4c6172934.png)

 **Course Section Code:** <https://github.com/mschwarzmueller/nextjs-course-
code/tree/zz-nextjs-summary>

**Course Section Code (Meetups Project):**
<https://github.com/mschwarzmueller/nextjs-course-code/tree/zz-prj-nextjs-
summary>

 **Extra Files:** <https://github.com/mschwarzmueller/nextjs-course-
code/tree/zz-nextjs-summary-extra-files>

